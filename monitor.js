var count = 1;

/*
Constructor

Parameters:
where (String)	URL for the monitoring data that this Monitor should gather.

label (String)	If provided, label for the monitor as it prints to the console.

update (Integer) If provided, the data will be updated every _ minutes. If not provided, the data will not update.

updateCallback (Method or Array of Methods) If provided, will be called whenever the data is updated.

testbedInfo (Array or Method)	If provided, the Monitor will use this as a way to segment queries. 
								If it is not provided, the maximum data will be 500 (since monitoring site limites a single query to 500 items).

dataPerTestbed (Integer)	The amount of entires in the data each testbed creates. Used to segment queries. Default is 1.
*/
function Monitor(where, label, update, updateCallback, testbedInfo, dataPerTestbed) {
	this.ready = false;
	this.where = where;
	this.dataPerTestbed = dataPerTestbed || 1;
	this.testbedInfo = (__.isFunction(testbedInfo)) ? testbedInfo() : testbedInfo;

	this.callbacks = [this.finishCallback];
	if (updateCallback) {
		if (updateCallback.constructor === Array) {
			this.callbacks = this.callbacks.concat(updateCallback);
		}
		else {
			this.callbacks.push(updateCallback);
		}
	}

	if (label) {
		this.label = '"'+label+'"';
	}
	else {
		this.label = count++;
	}

	this.httpGet(this);

	if (update) {
		var sI = setInterval(function() {
			this.self.httpGet(this.self);
		}, (update*60000));

		sI.self = this;
	}
}

Monitor.prototype.httpGet = function httpGet(self) {
	var httpGet;
	self.lastParsed = self.parsed;
	self.parsed = [];
	try {
		if (self.testbedInfo && (self.testbedInfo.length * self.dataPerTestbed) > 500) {
			httpGet = [];
			var dataPer = Math.floor(500/self.dataPerTestbed);
			self.waitFor = Math.ceil(self.testbedInfo.length / dataPer);
			for (var i = 0; i < self.testbedInfo.length; i += dataPer) {
				var testbeds = '&testbed=';
				for (var j = 0; j < dataPer; j++) {
					testbeds += self.testbedInfo[i+j]+',';
					if (i+j == self.testbedInfo.length-1) {
						break;
					}
				}
				httpGet[i] = https.get(self.where+testbeds, self.httpCallback);
				httpGet[i].self = self;
			}
		}
		else {
			self.waitFor = 1;
			httpGet = https.get(self.where, self.httpCallback);
			httpGet.self = self;
		}
	}
	catch(err) {
		console.log(err);
		self.report = [];
		self.parsed = self.lastParsed;
	}
}

Monitor.prototype.httpCallback = function httpCallback(res) {
	var self = this.self;
	var raw = '';
	var gotoCallback = this.gotoCallback;

	res.setEncoding('utf8');
	res.on('data', function(chunck) {
		raw += chunck;
	});
	res.on('end', function() {
		try {
			self.parsed.push(JSON.parse(raw));
		}
		catch(err) {
			console.log(err);
			self.parsed = self.lastParsed;
		}
		self.callbacks[0](self);
	});
}

Monitor.prototype.finishCallback = function finishCallback(self) {
	if (self.parsed.length == self.waitFor) {
		self.report = [];
		__.each(self.parsed, function(parse) {
			self.report = self.report.concat(parse);
		});

		if (self.ready == false) {
			self.ready = true;
			console.log('Monitor '+self.label+' Ready');
		}
		else {
			console.log('Monitor '+self.label+' Updated');
		}

		for (var i = 1; i < self.callbacks.length; i++) {	
			self.callbacks[i](self.report);
		}
	}
}

/*
isReady

Returns true after the first time the Monitor as finished gathering data from the server.
*/
Monitor.prototype.isReady = function isReady() {
	return this.ready;
}

/*
Evaluate

Used for filtering through the monitoring data.

Parameters:
queries (Array)	An array of query arrays to filter the data by. This uses OR logic.

				Query arrays are formatted as follows:
				query[0] (String)	The object key to search for.
				query[1] (String)	If populated, the data is filtered by matching to this object. Otherwise, all values are printed.
				query[2] (String)	If the data is nested in the object, this is the key for that data.
				query[3] (String)	Used if the key should be something other than the attr parameter.
				query[4] (Boolean)	If true, then the above parameter is used as a key and the corresponding value is used as the label.
				query[5] (String)	If the data is nested in the object, this is the key for that data.
				query[6] (String)	If populated, then this object key will be searched for if the first key is not found.

extend (Object)	If populated, the results of evaluate are added to this object instead of a new object. Useful for concatinating queries.

objKey (String)	If provided, this will be the key inside of the testbeds object that will be used.

keyPair (String)	If populated, this string will be a key in the returned object with the results from the evaluation as the value.

printLong (Boolean)	If true, instead of returning individual values the entire test object is returned. (May be removed, as this isn't very useful.)
*/
Monitor.prototype.evaluate = function evaluate(queries, extend, objKey, keyPair, printLong) {
	var result = extend || {};
	var filtered = this.report;

	__.each(queries, function(query) {
		var attr = query[0]; 
		var value = query[1];
		var type = query[2];
		var label = query[3] || attr;
		var labelPath = query[4];
		var labelType = query[5];
		var altAttr = query[6];

		if (value) {
			filtered = __.filter(filtered, function(obj) {
				var objeValue;
				var returnThis = false;
				var thisLabel = label;

				var testbed = obj['testbeds'][0]['name'];
				if (obj['testbeds'][0][objKey]) {
					testbed = obj['testbeds'][0][objKey];
				}

				var obje = obj;
				if (type) {
					obje = obj[type];
					if (obje.constructor === Array) {
						obje = obje[0];
					}
				}
				
				var attri = attr;
				if (altAttr && !(obje[attr] && ((obje[attr].constructor === Array && obje[attr][0]) || (!obje[attr].constructor === Array && obje[attr])))) {
					attri = altAttr;
				}

				if (labelPath == true) {
					if (labelType) {
						thisLabel = obj[labelType][label];
					}
					else {
						thisLabel = obj[label];
					}
				}


				// If attribute is an array
				if (obje[attri] && obje[attri].constructor === Array) {
					// If given value is an array
					if (value.constructor === Array) {
						var found = false;
						var foundValues = [];
						__.each(value, function(val) {
							if (__.contains(obje[attri], val)) {
								foundValues.push(val);
								found = true;
							}
						});
						if (found) {
							objeValue = foundValues;
							returnThis = true;
						}
					}
					else if (__.contains(obje[attri], value)) {
						objeValue = value;
						returnThis = true;
					}
				}
				else {	// Attribute is not an array
					// If given value is an array
					if (value.constructor === Array) {
						if (__.contains(value, obje[attri])) {
							objeValue = obje[attri];
							returnThis = true;
						}
					}
					else if (obje[attri] == value) {
						objeValue = value;
						returnThis = true;
					}
				}
				if (returnThis) {
					if (printLong) {
						result[testbed] = obj;
					}
					else if (type !== 'testbeds') {
						if (result[testbed] == undefined) {
							result[testbed] = {};
						}
						if (keyPair) {
							if (result[testbed][keyPair] == undefined) {
								result[testbed][keyPair] = {};
							}
							result[testbed][keyPair][thisLabel] = objeValue;
						}
						else {
							result[testbed][thisLabel] = objeValue;
						}
					}
				}
				else {
					if (!keyPair) {
						delete result[testbed];
					}
				}
				return returnThis;
			});
		}
		else {
			__.each(filtered, function(obj) {
				var testbed = obj['testbeds'][0]['name'];
				if (obj['testbeds'][0][objKey]) {
					testbed = obj['testbeds'][0][objKey];
				}

				var attri = attr;
				if (altAttr && obj[altAttr]) {
					attri = altAttr;
				}

				var thisLabel = label;
				if (labelPath === true) {
					if (labelType) {
						thisLabel = obj[labelType][label];
					}
					else {
						thisLabel = obj[label];
					}
				}
				if (printLong) {
					result[testbed] = obj;
				}
				else if (type !== 'testbeds') {
					var obje = obj
					if (type) {
						obje = obj[type];
					}
					if (result[testbed] == undefined) {
						result[testbed] = {};
					}
					if (keyPair) {
						if (result[testbed][keyPair] == undefined) {
							result[testbed][keyPair] = {};
						}
						result[testbed][keyPair][thisLabel] = obje[attri];
					}
					else {
						result[testbed][thisLabel] = obje[attri];
					}
				}
				// TODO attr is an object 
			});
		}
	});

	return result;
};

module.exports = Monitor;

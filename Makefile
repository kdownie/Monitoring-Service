#
# Makefile to glue to the Emulab/Cloudlab (FreeBSD) environment
# Should only be run on ops.
#
INSTALLDIR=	$(DESTDIR)/usr/testbed/libexec/cluster-status
JSFILES=	main.js monitor.js utils.js
LIBJSFILES=	lib/underscore-min.js
CONFFILES=	settings.conf
PROG=		cluster-status

all:

install: install-files install-certs

install-files: all
	mkdir -m 755 -p $(INSTALLDIR) $(INSTALLDIR)/lib
	install -C -m 755 $(JSFILES) $(INSTALLDIR)
	install -C -m 755 $(LIBJSFILES) $(INSTALLDIR)/lib
	install -C -m 644 $(CONFFILES) $(INSTALLDIR)
	install -C -m 750 driver.sh $(DESTDIR)/usr/testbed/sbin/$(PROG)

install-certs:
	@echo "Make sure that certs/keys are installed in $(INSTALLDIR)/ssl"

clean:




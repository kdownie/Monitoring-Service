http = require('http');
events = require('events');
e = new events.EventEmitter();

var tests = [
	function() {
		var test = 'Connection Test';
		var callback = function(status, data) {
			end(status, test);
		}
		makeRequest(target, callback);
	},
	function() {
		var test = 'Basic JSON Test 1';
		var callback = function(status, data) {
			if (!status) {
				end(status, test, data);
				return;
			}
			try {
				var t = data['utah-pg'];
				var check = [t.status, t.rawPCsAvailable, t.rawPCsTotal, t.VMsAvailable, t.VMsTotal];
				if (check.indexOf(undefined) == -1) {
					end(status, test);
				} 
				else {
					throw('JSON formatted incorrectly');
				}
			}
			catch(err) {
				end(false, test, err);
			}
		}
		makeRequest(target, callback);
	}
]





// Drives the tests

var target = 'clnode056.clemson.cloudlab.us';
var activeTest = 0;

// If test is defined, only that test will run.
function runTests(test) {
	var start = test || 0;
	var end = test || (tests.length-1);
	activeTest = test || 0;
	for (var i = start; i <= end; i++) {
		e.on('start'+i, function() {
			tests[activeTest]();
		});
	}
	e.emit('start0');
}

function end(result, which, message) {
	var m = (message) ? ', Output: '+message : '';
	var w = which || ('Test '+activeTest);
	console.log(w+': '+((result == true) ? 'OK' : 'FAIL')+m);

	if (++activeTest == tests.length) {
		//process.exit();
	}
	else {
		e.emit('start'+activeTest);
	}
}

function makeRequest(where, callback, path) {
	var options = {
		host: where,
		path: (path) ? '/'+path : '/',
		port: '8080'
	}
	try {
		var httpGet = http.get(options, gatherData);
		httpGet.self = {callback: callback};
		httpGet.setTimeout(10000, function() {
			httpGet.abort();
			callback(false, 'URL timed out.');
		});
	}
	catch(err) {
		end(false, test, err);
	}
}

function gatherData(res) {
	var self = this.self;
	self.raw = '';

	res.setEncoding('utf8');
	res.on('data', function(chunck) {
		self.raw += chunck;
	}).on('end', function() {
		self.report = JSON.parse(self.raw);
		self.callback(true, self.report);
	});
}

for (var i = 0; i < 100; i++) {
	setTimeout(runTests, i*1000);
}


#!/bin/sh
#
# Driver for Keith's iMinds monitoring service
#

INSTALLDIR=/usr/testbed/libexec/cluster-status
NODEJS=/usr/local/bin/node

if [ ! -d "$INSTALLDIR" ]; then
    echo "*** Monitoring service not installed"
    exit 1
fi

if [ ! -x "$NODEJS" ]; then
    echo "*** node JS interpreter not installed"
    exit 1
fi

# XXX needs to be run from the directory as all paths are relative
cd $INSTALLDIR || {
    echo "*** Could not chdir to $INSTALLDIR"
    exit 1
}

# if running as root, don't
UID=`id -u`
SUDO=
if [ $UID -eq 0 ]; then
    SUDO="/usr/local/bin/sudo -u nobody"
fi

$SUDO $NODEJS main.js

# XXX we may need to do some mapping of exit codes to make daemonwrapper happy
if [ $? -ne 0 ]; then
    echo "*** $NODEJS exited, status=$?"
    exit $?
fi

exit 0


https = require('https');
http = require('http');
__ = require('./lib/underscore-min'); // _ is used by node, so underscore is bound to __
var url = require('url');
var fs = require('fs');
var utils = require('./utils');
var Monitor = require('./monitor');

var allTestbeds = null;

var settings = {};
loadSettings();

var httpServer = http.createServer(handleRequest).listen(settings.HTTP_port);
console.log('HTTP server running on port '+settings.HTTP_port);

try {
	var sKey = fs.readFileSync(settings.ssl_directory+settings.ssl_key);
	var sCert = fs.readFileSync(settings.ssl_directory+settings.ssl_certificate);
	var options = {
		key: sKey,
		cert: sCert
	};

	var httpsServer = https.createServer(options, handleRequest).listen(settings['HTTPS_port']);
	console.log('HTTPS server running on port '+settings['HTTPS_port']);
}
catch (err) {
	console.log('SSL keys in the ./ssl directory are required for HTTPS. HTTPS not started.');
}

function updateCallback(d) {
	allTestbeds = {};
	__.each(d, function(obj) {
		allTestbeds[obj.shortname] = obj;
	});
	setMonitors();
}

function getTestbeds() {
	return __.keys(allTestbeds);
}

var categories = 'testbedcategory='+settings.testbed_category;
var testbedInfo = new Monitor(settings.monitoring_site+'/testbed?'+categories, 'testbedInfo', 60, updateCallback)
var status, baremetal, plnodes, stats;

function setMonitors() {
	if (!status) {
		status = new Monitor(settings.monitoring_site+'/result/?last&testdefinitionname=aggregateTestbedStatus&'+categories, 'status', 5, null, getTestbeds);
		baremetal = new Monitor(settings.monitoring_site+'/result/?last&testdefinitionname=listResources&'+categories, 'baremetal', 5, null, getTestbeds);
		plnodes = new Monitor(settings.monitoring_site+'/result/?last&testdefinitionname=autonodelogin&', 'plnodes', 5, null, getTestbeds);
	}
}

function handleRequest(req, res) {
	try {
		console.log('Incoming connection');
		if (!testbedInfo || !testbedInfo.isReady() 
			|| !status || !status.isReady() 
			|| !baremetal || !baremetal.isReady() 
			|| !plnodes || !plnodes.isReady()) {
			console.log('Service not ready');
			return;
		}
		var response;
		res.setHeader("Access-Control-Allow-Origin", "*")
		res.setHeader("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE")
		res.setHeader("Access-Control-Allow-Headers", "X-Requested-With, Content-Type")

		var params = url.parse(req.url, true).query;

		if (params['recommend'] !== undefined) {
			console.log('Recommending');
			response = recommendTestbed(handleQuery({testbeds: params['testbeds'], status: 'SUCCESS'}));
		}
		else {
			response = handleQuery(params)
		}

		response = JSON.stringify(response);
		if (params['callback'] || req['callback']) {
			var callback = params['callback'] || req['callback'];
			response = callback+'('+response+')';
		}

		res.writeHead(200);
		res.end(response + '\n');
	}
	catch (err) {
		console.log('ERROR: '+err.message);
	}
}

function handleQuery(params) {
	var results = {};
	var monitorQuery = [];
	var printLong = false;
	var queryDefined = false;
	var testbeds = null;
	var useID = settings.default_names;
	if (params['names']) {
		if (params['names'] == 'iminds') {
			useID = 'name';
		}
		else if (params['names'] == 'urn') {
			useID = 'urn';
		}
		else if (params['names'] == 'geni') {
			useID = 'geni_id';
		}
	}

	try {
		__.each(params, function(v, k) {
			var whiteList = ['testbeds','duration','returnValue','startTime','stopTime','count'];
			if (whiteList.indexOf(k) !== -1) {
				var key = k, aKey = null, val = v;
				var resultsObj = ['duration','returnValue','startTime','stopTime','count'];
				var inResults = (__.contains(resultsObj,key) ? 'results' : null);

				if (val && val.indexOf(',') !== -1) { // User supplied comma separated values.
					val = val.split(',');
				}

				if (k == 'testbeds') {
					testbeds = val;
					var test = (v.constructor === Array) ? v[0] : v;

					if (unescape(v).substr(0,4) == 'urn:') {
						key = 'urn';
						useID = 'urn';
					}
					else {
						key = useID;
						aKey = 'name';
					}
					inResults = 'testbeds';
				}

				monitorQuery.push([key, val, inResults, null, false, null, aKey]); 
			}
		});


		if (!params['query'] || params.query.toLowerCase() == 'rawpcs') {
			// Raw PC Count Queries
			monitorQuery.push(['countRawAvailable', params['rawpcs'], 'results', 'rawPCsAvailable']);
			monitorQuery.push(['countRawTotal', params['rawpcs'], 'results', 'rawPCsTotal']);
			results = baremetal.evaluate(monitorQuery, results, useID);
			// Need to take rawPC off of the queries stack for future calls
			monitorQuery.splice(-2,2);
		}
		if (!params['query'] || params.query.toLowerCase() == 'vms') {
			// VM Count Queries
			monitorQuery.push(['countVmAvailable', params['vms'], 'results', 'VMsAvailable']);
			monitorQuery.push(['countVmTotal', params['vms'], 'results', 'VMsTotal']);
			results = baremetal.evaluate(monitorQuery, results, useID);
			// Need to take vm off of the queries stack for future calls
			monitorQuery.splice(-2,2);
		}
		if (!params['query'] || params.query.toLowerCase() == 'status') {
			monitorQuery.push(['testbed_health_percent', null, 'results', 'health']);
			monitorQuery.push(['summary', (params['status'] ? params['status'].toUpperCase() : null), null, 'status']);
			results = status.evaluate(monitorQuery, results, useID);
			// Need to take summary off of the queries stack for future calls
			monitorQuery.splice(-1,1);
		}

		// Planetlab Europe requires users specify a specific node.
		// This special case will return the status of each node.
		if ((params['testbeds'] && params.testbeds == 'ple') || (params['nodeStatus'] !== undefined)) {
			if (results['ple'] == undefined) {
				results['ple'] = {};
			}
			results = plnodes.evaluate([['summary', params['nodeStatus'], null, 'fixed_node_name', true, 'results']], results, useID, 'nodeStatus');
		}
	}
	catch(err) {
		console.log('ERROR: '+err.message);
		return ('An Error Occured.');
	}

	return results;
}

function recommendTestbed(d) {
	var data = d;
	var rec = {recommendations : []};
	var depth = 3;
	
	__.each(data, function(val, key) {
		for (var i = 0; i < depth; i++) {
			if (rec.recommendations.length == i || +rec.recommendations[i].rawPCs < +val.rawPCs) {
				val['testbeds'] = key;
				rec.recommendations.splice(i, 0, val);
				delete data.key;
				
				if (rec.recommendations.length > depth) {
					rec.recommendations.splice(depth,1);
				}
				break;
			}
		}
	});

	return rec;
}

function loadSettings() {
	var settingsFile;
	try {
		var settingsFile = fs.readFileSync('settings.conf', {encoding: 'utf8'});
	}
	catch (err) {
		console.log('Could not load settings.conf');
		console.log('Using default settings');
	}
	if (settingsFile) {
		try {
			__.each(settingsFile.split('\n'), function(line) {
				if (line == '' || line.charAt(0) == '#') {
					return;
				}
				var setting = line.split('=');
				if (setting[1].charAt(0) == "'") {
					settings[setting[0]] = setting[1].split("'")[1];
				}
				else {
					settings[setting[0]] = parseInt(setting[1]);
				}
			});
		}
		catch (err) {
			console.log('Error in settings file: '+err);
			console.log('Using default settings');
			settings = {};
		}
	}

	// Set up defaults if needed
	if (!settings['HTTP_port']) {settings['HTTP_port'] = 8080}
	if (!settings['HTTPS_port']) {settings['HTTPS_port'] = 8081}
	if (!settings['ssl_directory']) {settings['ssl_directory'] = 'ssl/'}
	if (!settings['ssl_key']) {settings['ssl_key'] = 'monitoring-key.pem'}
	if (!settings['ssl_certificate']) {settings['ssl_certificate'] = 'monitoring-cert.pem'}
	if (!settings['testbed_category']) {settings['testbed_category'] = 'cloudlab,emulab,instageni,exogeni,international_federation,iMinds,fed4fire,fed4fire_extra,felix'}
	if (!settings['monitoring_site']) {settings['monitoring_site'] = 'https://flsmonitor.fed4fire.eu/api/index.php/'}
	if (!settings['default_names']) {settings['default_names'] = 'geni_id'}
}
